from typing import Optional
import os
import requests
import redis
from fastapi import FastAPI
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter

# variables and classes
class Item(BaseModel):
    result: str
    crm: int
    source: str


class Message(BaseModel):
    message: str

main_url = os.environ['CRMESS_URL']
attempt = 5
attempt = os.environ['ATTEMPT']
main_url = 'http://'+main_url+':4000/v1/crm/validate/'


# redis
SERVER = redis.ConnectionPool(host='redis', port=6379, db=0)
memo = redis.Redis(connection_pool=SERVER)

app = FastAPI()

# DEBUG MODE

#if True:
#    main_url = 'http://localhost:4000/v1/crm/validate/'
#    memo = redis.Redis()


# functions
def get_data(url):
    try:
        retries = Retry(total=5,backoff_factor=0.1,status_forcelist=[ 500, 502, 503, 504 ])
        r = requests.Session()
        r.mount('http://', HTTPAdapter(max_retries=retries))
        r =  r.get(url, timeout=2)
        if r.status_code == 200:
            response = r.json()
            r.close
            if response['result'] == 'valid':
                return True
            elif response['result'] == 'invalid':
                return False
            elif response['result'] == 'not_found':
                return 'not_found'
            else:
                return 'Error'
        else:
            print('Erro de comunicação com a API: '+url)
            print('HTTP Error: '+str(r.status_code))
            return 'Error'
    except:
        return 'Error'

def crm_bypass(crm):
    try:
        i = 0
        response = 'Error'
        while response == 'Error' and i < int(attempt):
            response = get_data(main_url+str(crm))
            print(response)
            i += 1
        if response is True:
            memo.setex(crm, 25200, 'valid')
            return {"result":"valid","crm": crm,"source":"crmess"}
        elif response is False:
            return JSONResponse(status_code=200, content={"result":"invalid","crm": crm,"source":"crmess"})
        else:
            return JSONResponse(status_code=404, content={"result": "not_found","crm": crm})
    except:
        return JSONResponse(status_code=404, content={"result": "not_found","crm": crm,})


@app.get("/")
def validation():
    return {"status": "Running"}


# main
@app.get("/api/validation/{crm}", response_model=Item, responses={404: {"model": Message, "description": "Not Found"},
                                                                  200: {"description": "Result requested by validation of the CRM number in CRMESS","content": {"application/json": {"example": {"result": "valid","crm": "1", "source": "redis"}}}},
                                                                  422: {"description": "Api request malformed","content": {"application/json": {"example": {"url": "http://127.0.0.1:8000/api/validation/1", "CRM": "1" }}}}})

def crm_request(crm: int, q: Optional[str] = None):
    response = 'Error'
    if q == 'crmess-force':
        return crm_bypass(crm)
    try:
        if memo.get(crm).decode("utf-8") == 'valid':
            return {"result":"valid","crm": crm,"source":"redis"}
    except:
        i = 0
        while response == 'Error' and    i < int(attempt):
            response = get_data(main_url+str(crm))
            i += 1
        if response is True:
            memo.setex(crm, 25200, 'valid')
            return {"result":"valid","crm": crm,"source":"crmess"}
        elif response is False:
            return JSONResponse(status_code=200, content={"result":"invalid","crm": crm,"source":"crmess"})
        else:
            return JSONResponse(status_code=404, content={"result": "not_found","crm": crm})

