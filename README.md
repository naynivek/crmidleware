# CRMidleware Challenge
## Description
CRMidleware is a midleware software that makes smart requests to CRMess in order do provide a great experience with CRM requests online.

## Installation

### Locally
The CRMidleware need some resources to run locally:
-  CRMess
-  Redis
-  Docker Engine + Docker Compose

1. For run the environment, make a download form a clone of this repository.
```
git clone https://gitlab.com/naynivek/crmidleware.git 
```
2. After download the repository, just execute the docker-compose.yml on the root of the code.
```
docker-compose up
```

### Staging/Production
Is recommended that CRMidleware in production runs in microsservices, which means the Redis have to be a separated and dedicated resource.
To  run in staging/production, the code must be builded on registry repository than deployed in a container on Kubernetes or similar.
Example on GCP:
```
gcloud builds submit . --tag gcr.io/<Project-ID>/CRMidleware:latest --project <Project-ID>
```
Where <Project-ID> is your project name on GCP.

## Usage Locally
The application will use the port 8000 and can be via localhost

http://127.0.0.1:8000/

The API documentation is in this endpoint:

http://127.0.0.1:8000/docs#/

To  validate a CRM, just execute a GET request in this URL:

http://127.0.0.1:8000/api/validation/3

Where the "3" would be the CRM code.

The CRMidleware by default execute 10 attempting request to CRMess until receives a valid 200 HTTP answer, which means, that in fact the CRMess answer with the correct validation of the CRM, after that, the data is cached on Redis for 7 days dont needing another request on CRMess.

To force the request to CRMess (creating a bypass on redis), just execute a request with parameter `q=crmess-force`

http://127.0.0.1:8000/api/validation/7?q=crmess-force

## Roadmap
1. Improvements on request from the CRMess (Makes more smarter)
2. Flexibility of the key expiry time on Redis via ENV
3. Documentation improvements

## Authors and acknowledgment
[Kevin Yan - @naynivek](https://gitlab.com/naynivek)

## License
This project is licensed under the terms of the MIT license.
